// Programmers: Catherine Sauer
//          	  Eli Verbrugge
// Section: D
// File: Assignment 8 // Decrypter

#include "decrypt.h"

int main()
{
  // Variables.
  string m_strLine = "";
  string m_strText = "";

  // Feed the encrypted data into one variable.
  for(int line = 1;
      retrieveFileLine(m_strLine, "encrypted.dat", line) && (m_strLine != "");
      line++)
  {
    m_strText.append(m_strLine);
    m_strText.append("\n");
  }

  // Decrypt the data.
  removeSpeechPutrifaction(m_strText);
  removePunctuationMayhem(m_strText);
  reverseSentenceDiscombobulations(m_strText);
  reverseWordFinagling(m_strText);

  cout << m_strText << endl;

  // Output the decrypted data to a file.
  outputToFile(m_strText, "decrypted.dat");

  return 0;
}
