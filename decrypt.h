// Programmers: Catherine Sauer
//          	  Eli Verbrugge
// Section: D
// File: Assignment 8 // Decrypter

#ifndef DECRYPT_H
#define DECRYPT_H

#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <algorithm>

using namespace std;

// Function Prototypes.

// Description:	   Opens and retrieves the given line of the given file.
// Preconditions:  fileName specifies the directory that contains a file with
//             	   the specified name and includes the file type. content is
//             	   large enough to contain the file's first line.
// Postconditions: content contains the given line of the specified file.
//             	   Returns true if the data was successfully acquired from the
//             	   file, false otherwise.
bool retrieveFileLine(string &content, const char fileName[],
                      const int lineToRead);

// Description:	   Outputs the given content to the specified file.
// Preconditions:  None
// Postconditions: Returns true if the data was successfully written to the
//                 specified file, false otherwise. If a file with the given
//                 name does not exist, that file is created with the content
//                 as the first line of the file. The data in this file will
//                 erased and overwritten.
bool outputToFile(const string content, const char fileName[]);

// Description:	   Determines if the given phrase is present in the given
//                 content and where the phrase begins, if found.
// Preconditions:  None
// Postconditions: Returns true if searchFor is present anywhere in content
//                 and foundIndex is beginning index of searchFor in content,
//                 false otherwise and foundIndex is -1.
bool isFound(const string searchFor, const string content, int &foundIndex);

// Description:	   Removes the indicated amount from the given string.
// Preconditions:  startingIndex is within the length of content.
// Postconditions: content is modified to not include the given amount
//                 (substring).
void removeSubStr(const int startingIndex, const int length, string &content);

// Description:	   Removes the instances of "--umm--", "--errr--", "--like--",
//                 and "--nstuff--" from content.
// Preconditions:  None
// Postconditions: content is modified to not contain the instances of speech
//                 'putrifaction' (putrefaction).
void removeSpeechPutrifaction(string &content);

// Description:	   If a word has an apostrophe in it, it moves it back to its
//                 correct position by swapping it with its following letter or
//                 with the first letter-position of the word (if at the end
//                 of a word).
// Preconditions:  None
// Postconditions: content is modified to have proper apostrophe punctuation.
// Note: This is equivalent to the assignment's 'Punctuation Mahem'
//       encryption step.
void removePunctuationMayhem(string &content);

// Description:	   Removes the instances of certain words and replaces them
//                 with their correct counterparts.
// Preconditions:  None
// Postconditions: content is modified to have certain words replaced.
void reverseWordFinagling(string &content);

// Description:	   Breaks up text into several substrings and passes it to
//                 corresponding swapping functions.
// Preconditions:  None
// Postconditions: text is returned after it is appropriately swapped.
void reverseSentenceDiscombobulations(string &text);

// Description:	   Reverse Words
// Preconditions:  None
// Postconditions: will return the modified sentence
void evenSwap(string &content, string punctuation);

// Description:	   Swaps Word Pairs
// Preconditions:  None
// Postconditions: will return the swapped sentence
void oddSwap(string &content, string punctuation);

#endif
