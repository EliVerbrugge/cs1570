// Programmers: Catherine Sauer
//          	  Eli Verbrugge
// Section: D
// File: Assignment 8 // Decrypter

#include "decrypt.h"


bool retrieveFileLine(string &content, const char fileName[],
                      const int lineToRead)
{
  // Local Objects.
  ifstream inf;
  string dummy;

  // Open the input file stream.
  inf.open(fileName);
  // If the file is not opened correctly, set the flag and exit the function.
  if (!inf)
  {
    return false;
  }
  // Ignore the lines prior to the desired line.
  for(int index = 1; index < lineToRead; index++)
  {
    getline(inf, dummy);
  }
  // Read the desired line.
  getline(inf, content);
  // Close the input file stream.
  inf.close();
  // If the function was not terminated, return as execution was successful.
  return true;
}

bool outputToFile(const string content, const char fileName[])
{
  // Local Objects.
  ofstream outf;

  // Open the output file stream.
  outf.open(fileName, ios::trunc);
  // If the file is not opened correctly, set the flag and exit the function.
  if (!outf)
  {
    return false;
  }
  // Write to the file, erasing previous data.
  outf << content;
  // Close the output file stream.
  outf.close();
  // If the function was not terminated, return as execution was successful.
  return true;
}

bool isFound(const string searchFor, const string content, int &foundIndex)
{
  // Local variables.
  int nContentLength = content.length();
  int nSearchLength  = searchFor.length();

  // Search for searchFor in content.
  for (int index = 0;
       index <= (nContentLength - nSearchLength);
       index++)
  {
    // Search for the first letter of searchFor, then continue to see if
    // searchFor in its entirety is present at this point.
    if (content[index] == searchFor[0])
    {
      // Assume the current possibility is what we're searching for.
      bool bFoundPossibility = true;
      // Save the beginning index for this possibility.
      foundIndex = index;
      // Search for the rest of the word.
      for (int verify = index, wordIndex = 0;
           verify <= (nContentLength - nSearchLength) &&
           bFoundPossibility && wordIndex < nSearchLength;
           verify++, wordIndex++)
      {
        // Check if this possibility is still the word.
        if (content[verify] != searchFor[wordIndex])
        {
          bFoundPossibility = false;
          foundIndex = -1;
        }
      }

      // If the possibility was searchFor, finish searching.
      if (bFoundPossibility == true)
      {
        return true;
      }
    }
  }

  return false;
}

void removeSubStr(const int startingIndex, const int length, string &content)
{
  // Local variables.
  string lastPortion = "";
  string firstPortion = "";

  // Save the first and last portions of the given content, omitting the
  // indicated amount (substring).
  for(unsigned int index = (startingIndex + length);
      index < content.length();
      index++)
  {
    lastPortion += content[index];
  }
  for (int index = 0; index < startingIndex; index++)
  {
    firstPortion += content[index];
  }
  // Update the passed in content with its previous value, omitting the
  // indicated amount (substring).
  content = firstPortion + lastPortion;
}

void removeSpeechPutrifaction(string &content)
{
  // Local Variables.
  int nIndexToRemove = -1;
  string toRemove1 = "--umm--";
  string toRemove2 = "--errr--";
  string toRemove3 = "--like--";
  string toRemove4 = "--nstuff--";

  // Remove all of the "--umm--" instances.
  while (isFound(toRemove1, content, nIndexToRemove))
  {
    // Remove the word. Note: the + 1 here is to omit the space following the
    // word.
    removeSubStr(nIndexToRemove, toRemove1.length() + 1, content);
  }
  // Remove all of the "--errr--" instances.
  while (isFound(toRemove2, content, nIndexToRemove))
  {
    // Remove the word. Note: the + 1 here is to omit the space following the
    // word.
    removeSubStr(nIndexToRemove, toRemove2.length() + 1, content);
  }
  // Remove all of the "--like--" instances.
  while (isFound(toRemove3, content, nIndexToRemove))
  {
    // Remove the word. Note: the + 1 here is to omit the space following the
    // word.
    removeSubStr(nIndexToRemove, toRemove3.length() + 1, content);
  }
  // Remove all of the "--nstuff--" instances.
  while (isFound(toRemove4, content, nIndexToRemove))
  {
    // Remove the word. Note: the + 1 here is to omit the space following the
    // word.
    removeSubStr(nIndexToRemove, toRemove4.length() + 1, content);
  }
}

void removePunctuationMayhem(string &content)
{
  // Scan the given string for an apostrophe.
  for (unsigned int index = 0; index < content.length(); index++)
  {
    if (content[index] == '\'')
    {
      // Check if the apostrophe is the last character of the line or before
      // a space (therefore at the end of the word).
      if (index == content.length() - 1 || content[index + 1] == ' ')
      {
        // Determine the size of the word with an apostrophe at the end.
        int wordLength;
        for(wordLength = 0; content[index - wordLength] != ' '; wordLength++);

        // Swap the apostrophe with the first letter-position of the word.
        char temp = content[index];
        content[index] = content[index - wordLength + 1];
        content[index - wordLength + 1] = temp;
      }
      else
      {
        // Swap the apostrophe and following character.
        char temp = content[index];
        content[index] = content[index + 1];
        content[index + 1] = temp;
      }

      // Move past the current apostrophe to search for another one.
      // Note: This is again incremented immediately after by the for loop.
      // This is necessary to skip the same apostrophe that just moved
      // positions.
      index++;
    }
  }
}

void reverseWordFinagling(string &test)
{
  // Replace all instances of 'she' with 'he'.
  int found = -1;
  while(static_cast<int>(test.find(" she ", found+1)) != -1)
  {
    found = test.find(" she ", found+1);
    if(found >= 0)
    test.replace(found, 5, " he ");
  }

  // Replace instances of 'her' with 'his'.
  found = -1;
  while(static_cast<int>(test.find(" her ", found+1)) != -1)
  {
    found = test.find(" her ", found+1);
    if(found >= 0)
    test.replace(found, 5, " his ");
  }

  // Replace instances of 'my' with 'your'.
  found = -1;
  while(static_cast<int>(test.find("my", found+1)) != -1)
  {
    found = test.find("my", found+1);
    if(found >= 0)
    test.replace(found, 2, "your");
  }

  // Replace instances of 'there' with 'their'.
  found = -1;
  while(static_cast<int>(test.find(" there ", found+1)) != -1)
  {
    found = test.find(" there ", found+1);
    if(found >= 0)
    test.replace(found, 7, " their ");
  }

  // Replace instances of 'I' with 'you'. This accounts for punctuation.
  found = -1;
  while(static_cast<int>(test.find("I", found+1)) != -1)
  {
    found = test.find("I", found+1);
    if(found >= 0)
    if(test[found+1] == ' ' && test[found-1] == ' ')
      test.replace(found, 2, "you ");
    else if(test[found+1] == '.' && test[found-1] == ' ')
      test.replace(found, 2, "you.");
  }
  // Replace instances of 'She' with 'He' (note the capitalization).
  found = -1;
  while(static_cast<int>(test.find("She ", found+1) != -1))
  {
     found = test.find("She ", found+1);
     if(found >= 0)
     test.replace(found, 4, "He ");
  }
  // Replace instances of 'She' with 'He' (note the capitalization).

  found = -1;
  while(static_cast<int>(test.find(" i ", found+1) != -1))
  {
    found = test.find(" i ", found+1);
    if(found >= 0)
      test.replace(found, 2, " you");
  }
  // Replace instances of 'frogs' with 'penguins'.
  found = -1;
  while(static_cast<int>(test.find("frogs", found+1)) != -1)
  {
    found = test.find("frogs", found+1);
    if(found >= 0)
    test.replace(found, 5, "penguins");
  }

  // Replace instances of 'Frogs' with 'Penguins' (note the capitalization).
  found = -1;
  while(static_cast<int>(test.find("Frogs", found+1)) != -1)
  {
    found = test.find("Frogs", found+1);
    if(found >= 0)
    test.replace(found, 5, "Penguins");
  }
}

void reverseSentenceDiscombobulations(string &text)
{
  int prevSpot = 0;
  int sentenceIterator = 1; //because the first sentence is actually 1 (odd)
  string returnText = "";
  string punctuation;

  //running while we are still traversing the text
  while(prevSpot < text.length())
  {
    // Search for the end of a sentence by '.', '?', or '!'.
    int found = text.find(".", prevSpot + 1);
    punctuation = ".";
    if(found == -1)
    {
      found = text.find("!", prevSpot + 1);
      punctuation = "!";
    }
    if(found == -1)
    {
      found = text.find("?", prevSpot + 1);
      punctuation = "?";
    }

    string substring;

    //if there is a space starting off we should move up to account for it
    if(isspace(text[prevSpot]))
      prevSpot+= 2;

    //different logic for if it is the starting sentence (will have
    // no space before)
    if(found >= 0 && sentenceIterator == 1)
      substring = text.substr(prevSpot, found-(prevSpot-1))+"  ";
    else if(found >= 0)
      substring = text.substr(prevSpot+2, found-(prevSpot))+" ";

    //if an odd sentence
    if(sentenceIterator%2 == 1)
    {
      oddSwap(substring, punctuation);
      returnText.append(substring);
    }
    //if an even sentence
    else if(sentenceIterator %2==0)
    {
      evenSwap(substring, punctuation);
      returnText.append(substring);
    }
    sentenceIterator++;
    prevSpot = found;

  }
  //making sure that we dont have any extra lines following our output, due to too many lines being read
  size_t pos;
  while ((pos= returnText.find("\n ", 0)) != std::string::npos)
  {
      returnText.erase(pos, 1);
  }

  text = returnText;
  return;
}

void oddSwap(string &content, string punctuation)
{
  string words[251] = { "" };
  int prevSpot =  0;
  int wordCount = 0;
  int found = 0;
  //while we are still no reaching the max words or end of sentence
  while(prevSpot < content.length() && wordCount < 251)
  {
    //looking for a space following the one we are currently at

    found = content.find(" ", prevSpot+1);

    if(content.find(punctuation, prevSpot+1)<content.find(punctuation, prevSpot+1))
      found = content.find(punctuation, prevSpot+1);

    if(found>= 0)
    {
      string str = content.substr(prevSpot, found - (prevSpot));

      //if we dont start with a space add one so that we don't get words appended together
      //this will add an extra space at the beginning which I take care of below
      if(str[0]!= ' ')
        str = " " + str;

      //also include the word inbetween a space and punctuation
      str.erase(std::remove(str.begin(), str.end(), punctuation[0]), str.end());
      str.erase(std::remove(str.begin(), str.end(), '\n'), str.end());

      words[wordCount] = str;
      wordCount++;
    }
    prevSpot = found;

  }
  //swap the pairs of words
  for(int i = 0; i<250 && words[i]!=" ";i+=2)
  {
    //swaps words if they are not empty spaces
    if(words[i+1]!= " ")
    {
      string temp = words[i];
      words[i] = words[i+1];
      words[i+1] = temp;
    }
    //fixes the capitalization if we are at the beginning of a sentence
    if(i == 0)
    {
      if(words[i]!="I")
        words[i][1] = toupper(words[i][1]);
      if(words[i+1]!="I")
        words[i+1][1] = tolower(words[i+1][1]);
    }
  }

  content = "";
  //adding all the words back to the original array
  for(int i = 0; i<251;i++)
  {
    if(words[i]!="")
      content.append(words[i]);
  }
  content.insert(content.length()-1,punctuation);
  content.erase(0,1);
  if(content[0]!= ' ')
  content.append("\n");
}

void evenSwap(string &content, string punctuation)
{
  string words[251] = { "" };
  int prevSpot =  0;
  int wordCount = 0;
  int found = 0;
  //filling up an array of words
  while(prevSpot < content.length() && wordCount < 251)
  {
    found = content.find(" ", prevSpot+1);
    //also include the word inbetween a space and punctuation
    if(content.find(punctuation, prevSpot+1)<content.find(punctuation, prevSpot+1))
      found = content.find(punctuation, prevSpot+1);
    if(found>= 0)
    {
      //create a substring representing a word
      string str = content.substr(prevSpot, found - (prevSpot));

      if(str[0]!= ' ')
        str = " " + str;

      //remove the punctuation so we dont have to deal with swapping it around
      str.erase(std::remove(str.begin(), str.end(), punctuation[0]), str.end());
      str.erase(std::remove(str.begin(), str.end(), '\n'), str.end());

        //if we dont have an empty word, add it to the array of words
        words[wordCount] = str;

      wordCount++;
    }
    prevSpot = found;
  }
  //usually include the padding afterwards, but not for this count
  wordCount -= 1;

  if(wordCount % 2 == 1)
  {
    for(int i=0; i<words[wordCount/2].length();i+=2)
    {
      //running through the word in the middle and reversing it
      char t = words[wordCount/2][i];
      words[wordCount/2][i] = words[wordCount/2][words[wordCount/2].length()-1-i];
      words[wordCount/2][words[wordCount/2].length()-1-i] = t;
    }
    //fixing the fact that we swapped words and they do not have appropriate spacing
    words[wordCount/2] = " " + words[wordCount/2];
    words[wordCount/2].erase(words[wordCount/2].length()-1,1);
  }

  //swapping the first and last words of a sentence, and changing capitalization

  string temp = words[0];
  words[0] = words[wordCount-1];

  //I is the only case in which we do not change capitalization
  if(temp[1]!= 'I')
    temp[1] = tolower(temp[1]);

  words[wordCount-1] = temp;

  char c = words[0][1];
  words[0][1] = toupper(c);
  content = "";

  //adding all the words back to the original string
  for(int i = 0; i<251;i++)
  {
    if(words[i]!="")
      content.append(words[i]);
  }
  //if we have a large enough string, add the punctuation back to the end
  if(content.length()>0)
    content.insert(content.length()-1,punctuation);
  //remove the extra space at the end of the file
  content.erase(0,1);
  if(content[0]!= ' ')
  content.append("\n");
}
